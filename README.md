The authors of the project are Marc Mitjans and Miquel Junyent.

The project is located inside the “Final_Project” folder. It consists in the ROS implementation in C++ of an autonomous control system for the quadcopter Parrot AR.Drone 2.0. The goal of the project is to make the drone take off and align with a floor marker at a certain height. After several seconds, the drone starts turning searching for a wall marker, and once found it leaves the previous location to align itself to the second marker at a specific distance.

The core of the code is a finite-state machine implemented in “stateMachine.cpp”, which defines 6 different states (Landed, Takeoff, Landing, AlignFloorMark, AlignWallMark and SearchWallMark) and the transitions between them. The drone is controlled by a PID controller defined in “PIDcontroller.h” and used in controller.cpp, which publishes the final command in the “/cmd_vel” topic of the drone.

The state machine communicates with the specific operations in each state through ROS actions and services. The important ones to mention are the “takeoff” and “lander” services used in “takeoffServer.cpp” and “landServer.cpp”, the action in “alignController.cpp” used to align with the floor marker, the action in “searchWall.cpp” used to search the wall marker, and the action in “gotoWall.cpp” used to align with the wall marker.

